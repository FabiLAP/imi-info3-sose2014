package ItemStategies;

import gildedrose.Item;
import gildedrose.UpdateSellInStrategy;

public class OctocatSellInStrategy extends UpdateSellInStrategy {

	@Override
	public void updateSellIn(Item item) {
		item.sellIn++;	
	}

}
