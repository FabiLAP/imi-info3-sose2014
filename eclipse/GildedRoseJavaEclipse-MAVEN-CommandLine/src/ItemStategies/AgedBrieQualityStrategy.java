package ItemStategies;

import gildedrose.Item;
import gildedrose.UpdateQualityStrategy;

public class AgedBrieQualityStrategy extends UpdateQualityStrategy {
	
	@Override
	public void computeQuality(Item item) {
		item.quality++;
	}

	
	
}
