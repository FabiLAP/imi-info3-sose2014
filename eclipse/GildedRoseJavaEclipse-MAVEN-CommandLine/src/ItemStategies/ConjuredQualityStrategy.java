package ItemStategies;

import gildedrose.Item;
import gildedrose.UpdateQualityStrategy;

public class ConjuredQualityStrategy extends UpdateQualityStrategy {

	@Override
	public void computeQuality(Item item) {
		if(item.sellIn<=0){
			item.quality-=4;
		}
		else{
			item.quality-=2;
		}
	}
}
