import static org.junit.Assert.*;

import org.junit.Test;


public class NodeTest {

	@Test
	public void testCreateListWithOneElement() {
		Node<String> list = new Node<String>("a");
		assertEquals("a", list.toString());
	}
	
	@Test
	public void testCreateListWithTwoElements() {
		Node<String> list = new Node<String>("a", new Node<String>("b"));
		assertEquals("a, b", list.toString());
	}
	
	@Test
	public void testCreateListWithMoreElements() {
		Node<String> list = new Node<String>("a",new Node<>("b",new Node<>("c",new Node<>("d"))));
		assertEquals("a, b, c, d", list.toString());
	}
	
	@Test
	public void testDeleteFirstNodeFromList() {
		Node<String> list = new Node<String>("a", new Node<String>("b"));
		
		//list = Anchor of the List
		list = list.delete(list, "a");
		assertEquals("b", list.toString());
	}
	
	@Test
	public void testDeleteLastNodeFromList() {
		Node<String> list = new Node<String>("a", new Node<String>("b"));
		
		list = list.delete(list, "b");
		assertEquals("a", list.toString());
	}
	
	@Test
	public void testDeleteSingleNodeFromList() {
		Node<String> list = new Node<String>("a");
		
		// anchor need to be null after the only node is deleted
		list = list.delete(list, "a");
		assertEquals (null , list);
	}
	
	@Test
	public void testDeleteMiddleNodeFromList() {
		Node<String> list = new Node<String>("a",new Node<>("b",new Node<>("c",new Node<>("d"))));
		
		list = list.delete(list, "b");
		assertEquals("a, c, d", list.toString());
	}
	
	@Test
	public void testReverseWithOneNodes() {
		Node<String> list = new Node<String>("a");
		
		//list = Anchor of the List
		list = list.reverse(list);
		assertEquals("a", list.toString());
	}
	
	@Test
	public void testReverseWithTwoNodes() {
		Node<String> list = new Node<String>("a", new Node<String>("b"));
		
		list = list.reverse(list);
		assertEquals("b, a", list.toString());
	}
	
	@Test
	public void testReverseWithMoreNodes() {
		Node<String> list = new Node<String>("a",new Node<>("b",new Node<>("c",new Node<>("d"))));
		
		list = list.reverse(list);
		assertEquals("d, c, b, a", list.toString());
	}
	
}
