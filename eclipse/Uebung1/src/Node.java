
public class Node<T> {

		T data;
		Node<T> next;
		
	public Node(T data) {
		this.data = data;
		this.next = null;
	}
	
	public Node(T data, Node<T> next) {
		this.data = data;
		this.next = next;
	}
	
	@Override
	public String toString() {
		if (next == null) {
			return data.toString();
		} else {
			return data.toString() + ", " + next.toString();
		}
	}
	
	public Node<T> delete(Node<T> node, T string) {	
	      if(node.data.equals(string)) {
	    	  return node.next;	    	  
	      }
	      else {
	    	  node.next = delete(node.next, string);	    	  
	      }

	      return node;
	}	
	
	public Node<T> reverse(Node<T> node) {
		Node<T> previous = null;
		
	    while (node != null) {
	        Node<T> next = node.next;
	        node.next = previous;
	        previous = node;
	        node = next;
	    }
	    
	    return previous;
	}
}
